# How to run this workshop

1. Install the requirements (preferably in a new environment) `pip install -r requirements.txt`
2. Launch Jupyter Notebook (ensure you are in the how-to-dvc directory) `jupyter notebook`
3. Enjoy the show :D